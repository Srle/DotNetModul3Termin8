$(document).ready(function(){

	// podaci od interesa
	var host = "http://localhost:";
	var port = "61975/";
	var drzavaEndpoint = "api/drzava/";
	var gradEndpoint = "api/grad/";	
	var formAction = "Create";
	var editingId;
		
	//DRŽAVA:

	// prikaz države
	$("#btnDrzave").click(function(){
		// ucitavanje države
		var requestUrl = host + port + drzavaEndpoint;
		console.log("URL zahteva: " + requestUrl);
		$.getJSON(requestUrl, setDrzave);
	});

	// metoda za postavljanje rezisera u tabelu
	function setDrzave(data, status){
		console.log("Status: " + status);

		var $container = $("#data");
		$container.empty(); 
		
			// sakrivanje forme grada
			$("#formDiv2").css("display","none");
			$container.append(div);
		
		if (status == "success")
		{
			console.log(data);
			// ispis naslova
			var div = $("<div></div>");
			var h1 = $("<h1>Države</h1>");
			div.append(h1);
			// ispis tabele država
			var table = $("<table></table>");
			var header = $("<tr><td>Id</td><td>Ime</td><td>Internacionalni kod</td><td>Delete</td><td>Edit</td></tr>");
			table.append(header);
			for (i=0; i<data.length; i++)
			{
				// prikazujemo novi red u tabeli
				var row = "<tr>";
				// prikaz podataka
				var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Ime + "</td><td>" + data[i].InternacionalniKod + "</td>";
				// prikaz dugmadi za izmenu i brisanje
				var stringId = data[i].Id.toString();
				var displayDelete = "<td><button id=btnDeleteDrzava name=" + stringId + ">Delete</button></td>";
				var displayEdit = "<td><button id=btnEditDrzava name=" + stringId + ">Edit</button></td>";
				row += displayData + displayDelete + displayEdit + "</tr>";  
				table.append(row);
			}
			
			div.append(table);

			// prikaz forme
			$("#formDiv1").css("display","block");

			// ispis novog sadrzaja
			$container.append(div);
		}
		else 
		{
			var div = $("<div></div>");
			var h1 = $("<h1>Greška prilikom preuzimanja Države!</h1>");
			div.append(h1);
			$container.append(div);
		}
	};

	// dodavanje nove države
	$("#formDrzava").submit(function(e){
		// sprecavanje default akcije forme
		e.preventDefault();

		var drzavaIme = $("#imeDrzava").val();
		var drzavaKod = $("#kodDrzava").val();
		var httpAction;
		var sendData;
		var url;

		// u zavisnosti od akcije pripremam objekat država
		if (formAction === "Create") {
			httpAction = "POST";
			url = host + port + drzavaEndpoint;
			sendData = {
				"Ime": drzavaIme,
				"InternacionalniKod": drzavaKod
			};
		}
		else {
			httpAction = "PUT";
			url = host + port + drzavaEndpoint + editingId.toString();
			sendData = {
				"Id": editingId,
				"Ime": drzavaIme,
				"InternacionalniKod": drzavaKod
			};
		}
		
		console.log("Objekat za slanje:");
		console.log(sendData);

		$.ajax({
			url: url,
			type: httpAction,
			data: sendData
		})
		.done(function(data, status){
			formAction = "Create";
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		})

	});
	
	// pripremanje dogadjaja za brisanje države
	$("body").on("click", "#btnDeleteDrzava", deleteDrzava);

	// priprema dogadjaja za izmenu države
	$("body").on("click", "#btnEditDrzava", editDrzava);

	// brisanje države
	function deleteDrzava() {
		// izvlacimo {id}
		var deleteID = this.name;
		// saljemo zahtev 
		$.ajax({
			url: host + port + drzavaEndpoint + deleteID.toString(),
			type: "DELETE",
		})
		.done(function(data, status){
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		});

	};

	// izmena države
	function editDrzava(){
		// izvlacimo id
		var editId = this.name;
		// saljemo zahtev da dobavimo te države
		$.ajax({
			url: host + port + drzavaEndpoint + editId.toString(),
			type: "GET",
		})
		.done(function(data, status){
			$("#imeDrzava").val(data.Ime);
			$("#kodDrzava").val(data.InternacionalniKod);				
			editingId = data.Id;
			formAction = "Update";
		})
		.fail(function(data, status){
			formAction = "Create";
			alert("Desila se greska!");
		});

	};

	// osvezi prikaz tabele država
	function refreshTable() {
		// cistim formu
		$("#imeDrzava").val('');
		$("#kodDrzava").val('');		
		// osvezavam
		$("#btnDrzave").trigger("click");
	};

	//GRAD:

	// prikaz gradovi
	$("#btnGradovi").click(function(){
		// ucitavanje grada
		var requestUrl = host + port + gradEndpoint;
		console.log("URL zahteva: " + requestUrl);
		$.getJSON(requestUrl, setGradovi);
	});
	
	
		// prikaz gradova između dva broja stanovnika
	$("#btnPretraga").click(function(){
		// ucitavanje grada
		var requestUrlS = host + port + "api/grad?pocetak=" + $("#od").val()+ "&kraj=" + $("#do").val();
		console.log("URL zahteva: " + requestUrlS);
		$.getJSON(requestUrlS, setGradovi);
	});
	

	// metoda za postavljanje grada u tabelu
	function setGradovi(data, status){
		console.log("Status: " + status);

		var $container = $("#data");
		$container.empty(); 
		
			// sakrivanje forme države
			$("#formDiv1").css("display","none");
			$container.append(div);
		
		if (status == "success")
		{
			console.log(data);
			// ispis naslova
			var div = $("<div></div>");
			var h1 = $("<h1>Gradovi</h1>");
			div.append(h1);
			// ispis tabele gradova
			var table = $("<table></table>");
			var header = $("<tr><td>Id</td><td>Ime</td><td>Poštanski broj</td><td>Broj stanovnika</td><td>Država</td><td>Delete</td><td>Edit</td></tr>");
			table.append(header);
			for (i=0; i<data.length; i++)
			{
				// prikazujemo novi red u tabeli
				var row = "<tr>";
				// prikaz podataka
				var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Ime + "</td><td>" + data[i].PostanskiKod + 
				"</td><td>" + data[i].BrStanovnika + "</td><td>" + data[i].Drzava.Ime + "</td>";
				// prikaz dugmadi za izmenu i brisanje
				var stringId = data[i].Id.toString();
				var displayDelete = "<td><button id=btnDeleteGrad name=" + stringId + ">Delete</button></td>";
				var displayEdit = "<td><button id=btnEditGrad name=" + stringId + ">Edit</button></td>";
				row += displayData + displayDelete + displayEdit + "</tr>";  
				table.append(row);
			}
			
			div.append(table);
			
			//////////////////////////////////////////////////////////////////////////////////
			
			// ucitavanje države u select
			var requestUrl1 = host + port + drzavaEndpoint;
			console.log("URL zahteva: " + requestUrl1);
			$.getJSON(requestUrl1, MeniDrzave);
			
			// pravljenje padajućeg menija režisera ispod tabele filmova
			function MeniDrzave(data1, status)
			{
				console.log("Status: " + status);
			
				var meni = $("#selectDrzava");
				meni.empty();
			
				if (status == "success")
				{
					//pravljenje stavki menija države
					var stavke ="";
					for (i=0; i<data1.length; i++)
					{
						stavke += "<option value="+data1[i].Id +">" + data1[i].Ime + "</option>";
					}
					meni.append(stavke);
				}
				else 
				{
					var div = $("<div></div>");
					var h1 = $("<h1>Greška prilikom preuzimanja Države za meni!</h1>");
					div.append(h1);
					$container.append(div);
				}
		
			}
			////////////////////////////////////////////////////////////////
			
			// prikaz forme
			$("#formDiv2").css("display","block");

			// ispis novog sadrzaja
			$container.append(div);
		}
		else 
		{
			var div = $("<div></div>");
			var h1 = $("<h1>Greška prilikom preuzimanja Grada!</h1>");
			div.append(h1);
			$container.append(div);
		}
	};


	// dodavanje novog grada
	$("#formGrad").submit(function(e){
		// sprecavanje default akcije forme
		e.preventDefault();

		var gradIme = $("#imeGrad").val();
		var gradKod = $("#kodGrad").val();
		var gradBrst = $("#brstGrad").val();
		var gradDrzavaId = $("#selectDrzava").val();
		var httpAction;
		var sendData;
		var url;

		// u zavisnosti od akcije pripremam objekat film
		if (formAction === "Create") {
			httpAction = "POST";
			url = host + port + gradEndpoint;
			sendData = {
				"Ime": gradIme,
				"PostanskiKod": gradKod,
				"BrStanovnika": gradBrst,
				"DrzavaId": gradDrzavaId
			};
		}
		else {
			httpAction = "PUT";
			url = host + port + gradEndpoint + editingId.toString();
			sendData = {
				"Id": editingId,
				"Ime": gradIme,
				"PostanskiKod": gradKod,
				"BrStanovnika": gradBrst,
				"DrzavaId": gradDrzavaId
			};
		}
		
		console.log("Objekat za slanje:");
		console.log(sendData);

		$.ajax({
			url: url,
			type: httpAction,
			data: sendData
		})
		.done(function(data, status){
			formAction = "Create";
			refreshTableGrad();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		})

	});

	// pripremanje dogadjaja za brisanje grada
	$("body").on("click", "#btnDeleteGrad", deleteGrad);

	// priprema dogadjaja za izmenu grada
	$("body").on("click", "#btnEditGrad", editGrad);

	// brisanje grada
	function deleteGrad() {
		// izvlacimo {id}
		var deleteID = this.name;
		// saljemo zahtev 
		$.ajax({
			url: host + port + gradEndpoint + deleteID.toString(),
			type: "DELETE",
		})
		.done(function(data, status){
			refreshTableGrad();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		});

	};

	// izmena grada
	function editGrad(){
		// izvlacimo id
		var editId = this.name;
		// saljemo zahtev da dobavimo taj grad
		$.ajax({
			url: host + port + gradEndpoint + editId.toString(),
			type: "GET",
		})
		.done(function(data, status){
			$("#imeGrad").val(data.Ime);
			$("#kodGrad").val(data.PostanskiKod);		
			$("#brstGrad").val(data.BrStanovnika);			
			$("#selectDrzava").val(data.DrzavaId);					
			editingId = data.Id;
			formAction = "Update";
		})
		.fail(function(data, status){
			formAction = "Create";
			alert("Desila se greska!");
		});

	};

	// osvezi prikaz tabele gradova
	function refreshTableGrad() {
		// cistim formu

		$("#imeGrad").val('');
		$("#kodGrad").val('');		
		$("#brstGrad").val('');		
		// osvezavam
		$("#btnGradovi").trigger("click");
	};	
	
});



