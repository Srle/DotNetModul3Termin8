﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RadSaGradovima.Models
{
    public class Grad
    {
        public int Id { get; set; }
        [Required]
        public string Ime { get; set; }
        public int PostanskiKod { get; set; }
        public int BrStanovnika { get; set; }

        // Foreign Key
        public int DrzavaId { get; set; }
        // Navigation property
        public Drzava Drzava { get; set; }
    }
}