﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RadSaGradovima.Models
{
    public class RadSaGradovimaContext : DbContext
    {
        public RadSaGradovimaContext() : base("name=RadSaGradovimaContext") { }

        public DbSet<Grad> Gradovi { get; set; }
        public DbSet<Drzava> Drzave { get; set; }

    }
}