﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RadSaGradovima.Models
{
    public class DrzavaDTO
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public int Stanovnici { get; set; }
    }
}