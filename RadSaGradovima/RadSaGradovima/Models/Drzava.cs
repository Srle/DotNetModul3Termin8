﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RadSaGradovima.Models
{
    public class Drzava
    {
        public int Id { get; set; }
        [Required]
        public string Ime { get; set; }
        [StringLength(3)]
        public string InternacionalniKod { get; set; }
    }
}