﻿using RadSaGradovima.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RadSaGradovima.Interfaces
{
    public interface IGradRepository
    {
        IEnumerable<Grad> GetAll();
        Grad GetById(int id);
        void Add(Grad grad);
        void Update(Grad grad);
        void Delete(Grad grad);

    }
}