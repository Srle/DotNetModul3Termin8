﻿using RadSaGradovima.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RadSaGradovima.Interfaces
{
    public interface IDrzavaRepository
    {
        IEnumerable<Drzava> GetAll();
        IEnumerable<DrzavaDTO> GetAllWithPeople();
        Drzava GetById(int id);
        void Add(Drzava drzava);
        void Update(Drzava drzava);
        void Delete(Drzava drzava);

    }
}