﻿using RadSaGradovima.Interfaces;
using RadSaGradovima.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace RadSaGradovima.Repository
{
    public class GradRepository : IDisposable, IGradRepository
    {
        private RadSaGradovimaContext db = new RadSaGradovimaContext();


        public IEnumerable<Grad> GetAll()
        {
            return db.Gradovi.Include(d => d.Drzava);
        }

        public Grad GetById(int id)
        {
            return db.Gradovi.FirstOrDefault(p => p.Id == id);
        }

        public void Add(Grad grad)
        {
            db.Gradovi.Add(grad);
            db.SaveChanges();
        }

        public void Update(Grad grad)
        {
            db.Entry(grad).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(Grad grad)
        {
            db.Gradovi.Remove(grad);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}