﻿using RadSaGradovima.Interfaces;
using RadSaGradovima.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace RadSaGradovima.Repository
{
    public class DrzavaRepository : IDisposable, IDrzavaRepository
    {
        private RadSaGradovimaContext db = new RadSaGradovimaContext();


        public IEnumerable<Drzava> GetAll()
        {
            return db.Drzave;
        }

        //pravi listu država dro sa brjem stanovnika
        public IEnumerable<DrzavaDTO> GetAllWithPeople()
        {
            var svigradovi = db.Gradovi.ToList();
            var svedrzave = db.Drzave.ToList();
            var drzaveDTO = new List<DrzavaDTO>();
            int stanovnici = 0;

            for (int i = 0; i < svedrzave.Count; i++)
            {
                for (int j = 0; j < svigradovi.Count; j++)
                {
                    if (svedrzave[i].Id == svigradovi[j].DrzavaId)
                    {
                        stanovnici += svigradovi[j].BrStanovnika;
                    }
                }
                drzaveDTO.Add(new DrzavaDTO { Id = svedrzave[i].Id, Ime = svedrzave[i].Ime, Stanovnici = stanovnici });
                stanovnici = 0;
            }
            return drzaveDTO;
        }


        public Drzava GetById(int id)
        {
            return db.Drzave.FirstOrDefault(p => p.Id == id);
        }

        public void Add(Drzava drzava)
        {
            db.Drzave.Add(drzava);
            db.SaveChanges();
        }

        public void Update(Drzava drzava)
        {
            db.Entry(drzava).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(Drzava drzava)
        {
            db.Drzave.Remove(drzava);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}