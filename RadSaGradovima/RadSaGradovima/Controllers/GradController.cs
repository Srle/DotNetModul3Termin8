﻿using RadSaGradovima.Interfaces;
using RadSaGradovima.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RadSaGradovima.Controllers
{
    public class GradController : ApiController
    {
        IGradRepository _repository { get; set; }

        public GradController(IGradRepository repository)
        {
            _repository = repository;
        }

        //Sortira sve gradove po poštanskom kodu radstuće
        public IEnumerable<Grad> Get()
        {
            return _repository.GetAll().OrderBy(x => x.PostanskiKod);
        }

        public IHttpActionResult Get(int id)
        {
            var grad = _repository.GetById(id);
            if (grad == null)
            {
                return NotFound();
            }
            return Ok(grad);
        }

        //Pretraga gradova sa brojem stanovnika između pocetak i kraja
        // GET /api/grad?pocetak=1000&kraj=1000000
        public IEnumerable<Grad> Get(int pocetak, int kraj)
        {
            List<Grad> lista = new List<Grad>();

            foreach (Grad grad in _repository.GetAll())
            {
                if (grad.BrStanovnika > pocetak && grad.BrStanovnika < kraj)
                {
                    lista.Add(grad);
                }
            }
            Grad[] nekigradovi = new Grad[lista.Count];

            for (int i = 0; i < lista.Count; i++)
            {
                nekigradovi[i] = lista[i];
            }

            return nekigradovi;
        }


        public IHttpActionResult Post(Grad grad)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(grad);
            return CreatedAtRoute("DefaultApi", new { id = grad.Id }, grad);
        }

        public IHttpActionResult Put(int id, Grad grad)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != grad.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(grad);
            }
            catch
            {
                return BadRequest();
            }
           
            return Ok(grad);
        }

        public IHttpActionResult Delete(int id)
        {
            var grad = _repository.GetById(id);
            if (grad == null)
            {
                return NotFound();
            }

            _repository.Delete(grad);
            return Ok();
        }

    }

}

