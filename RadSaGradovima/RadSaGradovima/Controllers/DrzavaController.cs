﻿using RadSaGradovima.Interfaces;
using RadSaGradovima.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RadSaGradovima.Controllers
{
    public class DrzavaController : ApiController
    {
        IDrzavaRepository _repository { get; set; }

        public DrzavaController(IDrzavaRepository repository)
        {
            _repository = repository;
        }


        public IEnumerable<Drzava> Get()
        {
            return _repository.GetAll();
        }


        //poziva metodu iz repozitorijuma za pravljenje svih država sa brojem stanovnika
        //i sortira ih po broju stanovnika opadajuće
        [Route("api/drzava/statistics")]
        public IEnumerable<DrzavaDTO> GetStanovnici()
        {
            return _repository.GetAllWithPeople().OrderByDescending(x => x.Stanovnici); 

        }


        public IHttpActionResult Get(int id)
        {
            var drzava = _repository.GetById(id);
            if (drzava == null)
            {
                return NotFound();
            }
            return Ok(drzava);
        }

        public IHttpActionResult Post(Drzava drzava)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(drzava);
            return CreatedAtRoute("DefaultApi", new { id = drzava.Id }, drzava);
        }

        public IHttpActionResult Put(int id, Drzava drzava)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != drzava.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(drzava);
            }
            catch
            {
                return BadRequest();
            }

            return Ok(drzava);
        }

        public IHttpActionResult Delete(int id)
        {
            var drzava = _repository.GetById(id);
            if (drzava == null)
            {
                return NotFound();
            }

            _repository.Delete(drzava);
            return Ok();
        }

    }
}
