﻿namespace RadSaGradovima.Migrations
{
    using RadSaGradovima.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RadSaGradovima.Models.RadSaGradovimaContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RadSaGradovima.Models.RadSaGradovimaContext context)
        {

            context.Drzave.AddOrUpdate(x => x.Id,
                new Drzava()
                { Id = 1, Ime = "Srbija", InternacionalniKod = "SRB" },
                new Drzava()
                { Id = 2, Ime = "Austrija", InternacionalniKod = "AUS" },
                new Drzava()
                { Id = 3, Ime = "Francuska", InternacionalniKod = "FRA" }
                );

            context.Gradovi.AddOrUpdate(x => x.Id,
                new Grad()
                { Id = 1, Ime = "Beograd", PostanskiKod = 11000, BrStanovnika = 1370000, DrzavaId = 1 },
                new Grad()
                { Id = 2, Ime = "Beč", PostanskiKod = 1220, BrStanovnika = 1730000, DrzavaId = 2 },
                new Grad()
                { Id = 3, Ime = "Novi Sad", PostanskiKod = 21000, BrStanovnika = 287000, DrzavaId = 1 },
                new Grad()
                { Id = 4, Ime = "Pariz", PostanskiKod = 75001, BrStanovnika = 2250000, DrzavaId = 3 }
                );
        }
    }
}
