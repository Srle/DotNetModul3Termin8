﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RadSaGradovima.Controllers;
using RadSaGradovima.Interfaces;
using RadSaGradovima.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace RadSaGradovima.Tests.Controllers
{
    [TestClass]
    public class GradConrtollerTest
    {
        [TestMethod]
        public void GetReturnsGradWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<IGradRepository>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new Grad { Id = 42 });

            var controller = new GradController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Grad>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);
        }

        // --------------------------------------------------------------------------------------

        [TestMethod]
        public void GetReturnsNotFound()
        {
            // Arrange
            var mockRepository = new Mock<IGradRepository>();
            var controller = new GradController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void DeleteReturnsNotFound()
        {
            // Arrange 
            var mockRepository = new Mock<IGradRepository>();
            var controller = new GradController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        // --------------------------------------------------------------------------------------

        [TestMethod]
        public void DeleteReturnsOk()
        {
            // Arrange
            var mockRepository = new Mock<IGradRepository>();
            mockRepository.Setup(x => x.GetById(10)).Returns(new Grad { Id = 10 });
            var controller = new GradController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        // --------------------------------------------------------------------------------------

        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<IGradRepository>();
            var controller = new GradController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Put(10, new Grad { Id = 9, Ime = "Grad2", BrStanovnika=1000, PostanskiKod=2222, DrzavaId=1 });

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        // -------------------------------------------------------------------------------------

        [TestMethod]
        public void PostMethodSetsLocationHeader()
        {
            // Arrange
            var mockRepository = new Mock<IGradRepository>();
            var controller = new GradController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Post(new Grad { Id = 10, Ime = "Grad1", BrStanovnika = 1000, PostanskiKod = 2222, DrzavaId = 1 });
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<Grad>;

            // Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
            Assert.AreEqual(10, createdResult.RouteValues["id"]);
        }

        // ------------------------------------------------------------------------------------------

        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            // Arrange
            List<Grad> gradovi = new List<Grad>();
            gradovi.Add(new Grad { Id = 1, Ime = "Grad1", BrStanovnika = 1000, PostanskiKod = 2222, DrzavaId = 1 });
            gradovi.Add(new Grad { Id = 2, Ime = "Grad2", BrStanovnika = 1000, PostanskiKod = 2222, DrzavaId = 1 });

            var mockRepository = new Mock<IGradRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(gradovi.AsEnumerable());
            var controller = new GradController(mockRepository.Object);

            // Act
            IEnumerable<Grad> result = controller.Get();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(gradovi.Count, result.ToList().Count);
            Assert.AreEqual(gradovi.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(gradovi.ElementAt(1), result.ElementAt(1));
        }

    }
}
