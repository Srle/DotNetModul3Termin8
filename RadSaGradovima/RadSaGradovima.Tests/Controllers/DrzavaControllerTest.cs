﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RadSaGradovima.Controllers;
using RadSaGradovima.Interfaces;
using RadSaGradovima.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Moq;


namespace RadSaGradovima.Tests.Controllers
{
    [TestClass]
    public class DrzavaControllerTest
    {
        [TestMethod]
        public void GetReturnsDrzavaWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<IDrzavaRepository>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new Drzava { Id = 42 });

            var controller = new DrzavaController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Drzava>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);
        }

        // --------------------------------------------------------------------------------------

        [TestMethod]
        public void GetReturnsNotFound()
        {
            // Arrange
            var mockRepository = new Mock<IDrzavaRepository>();
            var controller = new DrzavaController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void DeleteReturnsNotFound()
        {
            // Arrange 
            var mockRepository = new Mock<IDrzavaRepository>();
            var controller = new DrzavaController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        // --------------------------------------------------------------------------------------

        [TestMethod]
        public void DeleteReturnsOk()
        {
            // Arrange
            var mockRepository = new Mock<IDrzavaRepository>();
            mockRepository.Setup(x => x.GetById(10)).Returns(new Drzava { Id = 10 });
            var controller = new DrzavaController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        // --------------------------------------------------------------------------------------

        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<IDrzavaRepository>();
            var controller = new DrzavaController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Put(10, new Drzava { Id = 9, Ime = "Drzava2", InternacionalniKod ="SSS" });

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        // -------------------------------------------------------------------------------------

        [TestMethod]
        public void PostMethodSetsLocationHeader()
        {
            // Arrange
            var mockRepository = new Mock<IDrzavaRepository>();
            var controller = new DrzavaController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Post(new Drzava { Id = 10, Ime = "Drzava1", InternacionalniKod = "SSS" });
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<Drzava>;

            // Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
            Assert.AreEqual(10, createdResult.RouteValues["id"]);
        }

        // ------------------------------------------------------------------------------------------

        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            // Arrange
            List<Drzava> drzave = new List<Drzava>();
            drzave.Add(new Drzava { Id = 1, Ime = "Drzava1", InternacionalniKod = "SSS" });
            drzave.Add(new Drzava { Id = 2, Ime = "Drzava2", InternacionalniKod = "SSS" });

            var mockRepository = new Mock<IDrzavaRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(drzave.AsEnumerable());
            var controller = new DrzavaController(mockRepository.Object);

            // Act
            IEnumerable<Drzava> result = controller.Get();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(drzave.Count, result.ToList().Count);
            Assert.AreEqual(drzave.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(drzave.ElementAt(1), result.ElementAt(1));
        }

    }
}
